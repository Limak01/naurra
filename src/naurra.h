#include <stdlib.h>
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/grid.h>

class Naurra : public Gtk::Window {
public:
	Naurra();

private:
	void close_app();
	void logout();
	void shutdown();
	void reboot();
	void suspend();
	void hibernate();
	void lock();

	Gtk::Grid grid;
	Gtk::Button close_btn, logout_btn, shutdown_btn, reboot_btn, suspend_btn, hibernate_btn, lock_btn;
};