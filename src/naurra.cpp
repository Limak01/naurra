#include "naurra.h"

Naurra::Naurra() {
	set_title("Naurra");
	set_default_size(400, 100);

	//Set button labels
	close_btn.set_label("Cancel");
	logout_btn.set_label("Logout");
	shutdown_btn.set_label("Shutdown");
	reboot_btn.set_label("Reboot");
	suspend_btn.set_label("Suspend");
	hibernate_btn.set_label("Hibernate");
	lock_btn.set_label("Lock");

	grid.set_row_spacing(10);


	//Attach buttons to grid
	grid.attach(close_btn, 0, 0, 1, 1);
	grid.attach(logout_btn, 0, 1, 1, 1);
	grid.attach(shutdown_btn, 0, 2, 1, 1);
	grid.attach(reboot_btn, 0, 3, 1, 1);
	grid.attach(suspend_btn, 0, 4, 1, 1);
	grid.attach(hibernate_btn, 0, 5, 1, 1);
	grid.attach(lock_btn, 0, 6, 1, 1);


	//Add click listeners to buttons
	close_btn.signal_clicked().connect(sigc::mem_fun(*this, &Naurra::close_app));
	logout_btn.signal_clicked().connect(sigc::mem_fun(*this, &Naurra::logout));
	shutdown_btn.signal_clicked().connect(sigc::mem_fun(*this, &Naurra::shutdown));
	reboot_btn.signal_clicked().connect(sigc::mem_fun(*this, &Naurra::reboot));
	suspend_btn.signal_clicked().connect(sigc::mem_fun(*this, &Naurra::suspend));
	hibernate_btn.signal_clicked().connect(sigc::mem_fun(*this, &Naurra::hibernate));
	lock_btn.signal_clicked().connect(sigc::mem_fun(*this, &Naurra::lock));

	//Align grid in the middle of the window
	grid.set_halign(Gtk::Align::CENTER);
	grid.set_valign(Gtk::Align::CENTER);

	//Show box in window
	set_child(grid);
}

void Naurra::close_app() {
	hide();
}

void Naurra::logout() {
	system("killall xmonad-x86_64-linux");
}

void Naurra::shutdown() {
	system("shutdown -h now");
}

void Naurra::reboot() {
	system("reboot");
}

void Naurra::suspend() {
	system("systemctl suspend");
}

void Naurra::hibernate() {
	system("systemctl hibernate");
}

void Naurra::lock() {
	system("slock");
}